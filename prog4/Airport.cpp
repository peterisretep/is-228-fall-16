// Airport.cpp

#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>

using namespace std;

void Airport(
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segs. needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
        int simulationLength// Total number of time segs. to simulate
        ) {
    queue<int> takeoffQ, landingQ;
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0; // starting values are set to 0
    int numLandings = 0, numTakeoffs = 0;

    srand(time(NULL));

    for (int curTime = 0; curTime < simulationLength; curTime++) {  // if the current time segment is less than the simulations length increment the time segment

        if (((double) rand() / (double) RAND_MAX) < arrivalProb) { // divide the rand number by it's allowed maximum value
                                                                    // if it is less than the arrival probability

            landingQ.push(curTime); // show the current time in the landing queue 
 
       }
        if (((double) rand() / (double) RAND_MAX) < takeoffProb) { 

            takeoffQ.push(curTime); // same as above but used for take off probability 

        }

        if (timeFree <= curTime) { // if the time in the queue is less than or equal to the current queue time 

            if (!landingQ.empty()) { // if there is space in the landing queue keep executing

                if ((curTime - landingQ.front()) > maxTime) { 
                    crashes++; // if the cur time minus the front of the queue is greater than the max time,
                    landingQ.pop();  //  decrease the size of the queue with pop and increment the crash counter
                } else {        // if there is no crash execute this else
                    landingWaits += (curTime - landingQ.front()); 
                    landingQ.pop(); // decrement the landing queue by 1 after popping
                    timeFree += landingTime;
                    numLandings++; // the plane had enough time in the seg to land, increment landing counter by 1
                }

            }
            else if (!takeoffQ.empty()) { 

                takeoffWaits += (curTime - takeoffQ.front());
                takeoffQ.pop(); // decrement the size of the take off queue
                timeFree += takeoffTime;
                numTakeoffs++; // increment the take off counter by 1

            }

        }

    }

    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime) {
        crashes++; // runs at the end of simulation to make sure all planes are accounted for in the queue
        landingQ.pop(); // decrements plane from queue if any have crashed
    }

    cout << "Number of crashes: " << crashes << "\n";
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " <<
            ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n";
    cout << "Number of landings: " << numLandings << " (avg delay " <<
            ((double) landingWaits) / ((double) numLandings) << ")\n";
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n";
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n";

}
