// LinkedList.cpp

#include <stdlib.h>
#include "LinkedList.h"


// Default constructor, the list is empty to start

LinkedList::LinkedList() {
    head = NULL;
    length = 0;

}

// Default destructor, must delete all nodes

LinkedList::~LinkedList() {
    Node* temp;
    Node* head_n = head;

    while (head_n != NULL) { //as long as the head value is not null, run the loop
        temp = head_n -> next; // set temp to point to node after head
        delete head_n; // delete the head node
        head_n = temp; // head node now is equal to temp

    }
}

// Add a node containing "value" to the front

void LinkedList::InsertFront(double value) {
    Node *fresh;
    fresh = new Node;
    fresh -> data = value;
    fresh -> next = head;
    head = fresh;
    length++;

}

// Add a node containing "value" to position "index"

void LinkedList::Insert(double value, unsigned int index) {
    Node *fresh, *prev;
    fresh = new Node; //creates new node called fresh
    prev = head; //sets prev pointer to head

    if (index > length)
        throw "ERROR";
    if (index == 0) //if index is == to 0
        InsertFront(value); // insert value to front
    else {
        for (int n = 0; n < index - 1; n++) { // runs loop through index
            prev = prev -> next;
        }
        fresh -> data = value; //points fresh node to data equal to value
        fresh -> next = prev -> next;
        prev -> next = fresh;
        length++;


    }
}

// Return value at position "index"

double LinkedList::GetNode(unsigned int index) {
    Node* cur = head;

    if (index >= length)
        throw "ERROR";
    if (index <= 0)
        throw "ERROR";
    else if (cur == NULL)
        throw "ERROR";
    else {
        for (int n = 0; n < index; n++) {
            cur = cur -> next;
        }
        return cur -> data;
    }
}


// Return the index of the node containing "value"

unsigned int LinkedList::Search(double value) {
    Node* cur = head;

    for (int n = 0;; n++) { //I need to search and return the location
        if (cur == NULL)
            throw "ERROR";
        else if (cur -> data == value)
            return n; //changed from next to data
        else
            cur = cur -> next; //if the data it's searching for does not equal the value it points to next node
        //return value;

    }
}


// Delete the node at position "index", return the value that was there

double LinkedList::DeleteNode(unsigned int index) {

    prev -> next = head
            delete = delete
            length--; //  no clue what to do with this
}

// This function reverses the order of all nodes so the first node is now the
// last and the last is now the first (and all between). So a list containing 
// (4, 0, 2, 11, 29) would be (29, 11, 2, 0, 4) after Reverse() is called.

void LinkedList::Reverse() {
    Node *temp_n, *cur_n = head;
    //delete the node at index 0, then insert it at the end, then delete node at 0 again, and insert at the end
    //make a counter to increment through about 5 times
    while (cur_n != NULL) { //while the cur_n (head) is not equal to null run the loop)
        cur_n = head; // sets cur_n to head
        cur_n = cur_n -> next; //sets the cur_n to the node after
        temp_n = head; // makes a temp ptr that is now set as the head
        head = cur_n -> next; // the head/"temp" ptr is set to the node after cur_n
        cur_n -> next = temp_n; // the node after cur_n is now set to temp

    }
}

// Return the number of nodes in the list

int LinkedList::GetLength() {
    
    while(length != 0 or == length)
        length ++;
    return length;
    
    
}

